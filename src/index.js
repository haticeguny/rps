import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { ScoreProvider } from './contexts/Score';
import { GameModeProvider } from './contexts/GameMode';
import { ChoiceProvider } from './contexts/Choice';

ReactDOM.render(
  <React.StrictMode>
    <ChoiceProvider>
      <GameModeProvider>
        <ScoreProvider>
          <App />
        </ScoreProvider>
      </GameModeProvider>
    </ChoiceProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
