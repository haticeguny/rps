import React, {
  createContext, useState, useEffect, useContext
} from 'react';

const GameMode = createContext({});

export const GameModeProvider = ({ children }) => {
  const [gameMode, setGameMode] = useState(0);

  const getGameMode = () => {
    // get GameMode from local storage
    let initialGameMode = '';
    if(localStorage.getItem('gameMode')) {
      initialGameMode = localStorage.getItem('gameMode');
    }

    localStorage.setItem("gameMode", initialGameMode);
    setGameMode(initialGameMode);
  };

  const updateGameMode = (updatedGameMode) => {
    localStorage.setItem("gameMode", updatedGameMode);
    setGameMode(updatedGameMode);
  };

  useEffect(() => {
    getGameMode();
  }, []);


  return (
    <GameMode.Provider value={{ gameMode, updateGameMode }}>
      {children}
    </GameMode.Provider>
  );
};

export const useGameMode = () => useContext(GameMode);

export default GameMode;
