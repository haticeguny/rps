import React, {
  createContext, useState, useEffect, useContext
} from 'react';

const Score = createContext({});

export const ScoreProvider = ({ children }) => {
  const [score, setScore] = useState(0);

  const getScore = () => {
    // get Score from local storage
    let initialScore = 0;
    if(localStorage.getItem('basicGameScore')) {
      initialScore = parseInt(localStorage.getItem('basicGameScore'), 10)
    }

    localStorage.setItem("basicGameScore", initialScore);
    setScore(initialScore);
  };

  const updateScore = (newScore, gameType) => {
    if(gameType === 'basic') {
      localStorage.setItem("basicGameScore", newScore);
    } else {
      localStorage.setItem("advancedGameScore", newScore);
    }

    setScore(newScore);
  };

  useEffect(() => {
    getScore();
  }, []);


  return (
    <Score.Provider value={{ score, updateScore }}>
      {children}
    </Score.Provider>
  );
};

export const useScore = () => useContext(Score);

export default Score;
