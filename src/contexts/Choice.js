import React, {
  createContext, useState, useEffect, useContext
} from 'react';

const Choice = createContext({});

export const ChoiceProvider = ({ children }) => {
  const [choice, setChoice] = useState(0);

  const getChoice = () => {
    // get Choice from local storage
    let initialChoice = '';
    setChoice(initialChoice);
  };


  useEffect(() => {
    getChoice();
  }, []);


  return (
    <Choice.Provider value={{ choice, setChoice }}>
      {children}
    </Choice.Provider>
  );
};

export const useChoice = () => useContext(Choice);

export default Choice;
