import './App.css';
import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import BasicChoices from './components/BasicChoices';
import AdvancedChoices from './components/AdvancedChoices';
import Result from './components/Result';
import GameHeader from './components/GameHeader';

import { useScore } from './contexts/Score';
import Game from './containers/Game';
import SwitchButton from './components/SwitchButton';
import ResetButton from './components/ResetButton';

function App() {
  const { score = 0, updateScore } = useScore();
  const [gameType, switchGameType] = useState('basic');
  
  return (
    <Router>
    <div className="App d-flex flex-column align-items-center" id="mainElementContainer">
      <div className="container" id="rpsElementsContainer">
        <GameHeader score={score} gameType={gameType} />
      <Switch>
          <Route path="/basic" exact={true}>
            <BasicChoices />
          </Route>
          <Route path="/basic/choice">
            <Result 
              userScore={score}
              updateUserScore={updateScore}
            />
          </Route>
          <Route path="/advanced" exact={true}>
            <AdvancedChoices />
          </Route>
          <Route path="/" exact={true}>
            <Game />
          </Route>
        </Switch>
          <div className="rpsRow" id={'rulesButtonsContainer'}>
            <div className="buttonsContainer">
              <SwitchButton
                gameType={gameType} 
                switchGameType={switchGameType} 
                updateUserScore={updateScore}
              />
              <ResetButton updateUserScore={updateScore}/>
            </div>
          </div>
      </div>
    </div>
    </Router>
  );
}

export default App;
