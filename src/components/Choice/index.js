import React from 'react';
import { withRouter } from "react-router-dom";


// assets
import paperIcon from '../../assets/svgs/icon-paper.svg';
import scissorIcon from '../../assets/svgs/icon-scissors.svg';
import rockIcon from '../../assets/svgs/icon-rock.svg';
import spockIcon from '../../assets/svgs/icon-spock.svg';
import lizardIcon from '../../assets/svgs/icon-lizard.svg';
import { useChoice } from '../../contexts/Choice';

import './index.css';

const Choice = (props) => {
  const {choice, setChoice} = useChoice();

  const renderImageComponent = (component) => {
    switch(component) {
      case 'paper':
        return(<img src={paperIcon} className={`circleIcon ${props.source}`} alt={'paper'}/>)
      case 'scissor':
          return(<img src={scissorIcon} className={`circleIcon ${props.source}`} alt={'scissor'}/>) 
      case 'rock':
          return(<img src={rockIcon} className={`circleIcon ${props.source}`} alt={'rock'}/>)
      case 'spock':
          return(<img src={spockIcon} className={`circleIcon ${props.source}`} alt={'rock'}/>)
      case 'lizard':
          return(<img src={lizardIcon} className={`circleIcon ${props.source}`} alt={'rock'}/>)
      default:
          return null;   
    }
  }
  
  const handleSelectChoice = (userChoice) => {
    props.history.push({
      pathname: '/basic/choice',
      state: { gameType: props.source }
    });

    setChoice(userChoice);
  }

  const outerCircleClass = `outerCircle ${props.componentName} ${props.source} hv-center`;
  let innerCircleClass = `innerCircle ${props.source} hv-center`;
  if(props.componentName === 'empty') {
      innerCircleClass += ` empty`;
  }

  return (
    <div className={outerCircleClass} onClick={() => handleSelectChoice(props.componentName)}>
      <div className={innerCircleClass}>{renderImageComponent(props.componentName)}</div>
    </div>
  )
}

export default withRouter(Choice);
