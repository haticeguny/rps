import React from 'react';
import { withRouter } from 'react-router-dom';
import './index.css';

const ResetButton = ({ updateUserScore, history }) => {
    const handleReset = () => {
      updateUserScore(0, 'update');

      history.push({
        pathname: "/",
      });
    }

    return(
        <div className="resetButton">
            <button type="button" className="btn lowerButton" onClick={handleReset}>
             <div className="hv-center">
                <i className={'fa fa-refresh mr-1'} aria-hidden="true"></i>
                <span>RESET</span>
             </div> 
            </button>
        </div>
    )
}

export default withRouter(ResetButton);
