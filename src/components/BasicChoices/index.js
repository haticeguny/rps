import React from 'react';
import Choice from '../Choice';
import './index.css';

const BasicChoices = () => (
  <div className="rpsRow">
    <div className="rpsContainer">
      <div className="rpsHalfContainer">
        <div className="rpsUpperLeftItem">
          <Choice
            componentName={'paper'}
            source={'basic'}
          />
        </div>
        <div className="rpsUpperRightItem">
          <Choice
            componentName={'scissor'}
            source={'basic'}
          />
        </div>
      </div>
      <div className="rpsHalfContainer hv-center">
        <Choice
            componentName={'rock'}
            source={'basic'}
        />
      </div>
    </div>  
  </div>
);

export default BasicChoices;
