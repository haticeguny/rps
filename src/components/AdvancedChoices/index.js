import React from 'react';
import Choice from '../Choice';
import './index.css';

const AdvancedChoices = () => (
  <div className="advancedChoices">
    <div className="advancedRPSLSContainer">
        <div className="advancedRPSLSTopContainer">
            <Choice 
              componentName={'scissor'}
              source={'advanced'}
            />
        </div>
        <div className="advancedRPSLSMiddleContainer">
            <Choice 
              componentName={'spock'}
              source={'advanced'}
            />
            <Choice 
              componentName={'paper'}
              source={'advanced'}
            />
        </div>
        <div className="advancedRPSLSLowerContainer">
            <Choice 
              componentName={'lizard'}
              source={'advanced'}
            />
            <Choice 
              componentName={'rock'}
              source={'advanced'}
            />
        </div>
    </div>
  </div>
);

export default AdvancedChoices;
