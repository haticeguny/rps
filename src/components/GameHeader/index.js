import React from 'react';
import './index.css';

const GameHeader = ({ score, gameType }) => {
  let gameOptions = ['ROCK', 'PAPER', 'SCISSORS'];  
  let optionsClass = 'basicOptions';
  if(gameType === 'advanced') {
      gameOptions = gameOptions.concat('LIZARD', 'SPOCK');
      optionsClass = 'advancedOptions';
  }

  return(
    <div className="rpsHeaderRow">
      <div className="header">
        <div className="headerRow">
          <div className="headerText">
            <div className={optionsClass}>
              {gameOptions.map((item, index)=>{
                return(
                    <div key={index}>
                        {item}
                    </div>
                )
              })}
            </div>
          </div>
          <div className="score">
            <div className="score-text">
                SCORE
            </div>
            <div className="score-value">
                {score}
            </div> 
          </div>
        </div>
      </div>
      </div>
  )
}

export default GameHeader;
