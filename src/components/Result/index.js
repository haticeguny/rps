import React, {useState, useEffect} from 'react';
import { withRouter } from "react-router-dom";

import { getComputersChoice, getGameResult, calculateGameScore } from '../../utils';
import Choice from '../Choice';

import { useScore } from '../../contexts/Score';
import { useChoice } from '../../contexts/Choice';
import { useGameMode } from '../../contexts/GameMode';

import './index.css';


const Result = (props) => {
    const [houseChoice, updateHouseChoice] = useState('empty');
    const [gameResult, updateGameResult] = useState(null);
    const gameType = props.location.state.gameType;
  
    const { score = 0, updateScore } = useScore();
    const { choice: userChoice, setChoice} = useChoice();
    const { gameMode } = useGameMode();

    useEffect(() => {
        const timer = setTimeout(() => {
            const randomHouseChoice = getComputersChoice(gameType);  
            updateHouseChoice(randomHouseChoice);

            if (gameMode === 'computer') {
                const randomUserChoice = getComputersChoice(gameType);  
                setChoice(randomUserChoice);
            }
        }, 800);

        return () => clearTimeout(timer);
    }, []);

    useEffect(() => {
        const timer = setTimeout(() => {
            if (userChoice === 'empty' && houseChoice === 'empty' && gameMode === 'computer') {
                const randomUserChoice = getComputersChoice(gameType);  
                setChoice(randomUserChoice);

                const randomHouseChoice = getComputersChoice(gameType);  
                updateHouseChoice(randomHouseChoice);
            }
        }, 800);

        return () => clearTimeout(timer);
    }, [houseChoice, userChoice])
    
    useEffect(() => {
        if(houseChoice !== 'empty') {
            const timer = setTimeout(() => {
                const gameResult = getGameResult(userChoice, houseChoice);
                const updatedScore = calculateGameScore(score, gameResult);

                updateScore(updatedScore, gameType);
                updateGameResult(gameResult);
            }, 800);
            return () => clearTimeout(timer);            
        }
    }, [houseChoice, userChoice]);

    const renderChoiceComponent = (componentName, text) => {
        return(
            <div className="rpsChoiceHalf hv-center">
                <div className="rpsChoiceHalfUpperSection hv-center">
                    <Choice 
                      componentName={componentName}
                      source={'basic'}
                    />
                </div>
                <div className="rpsChoiceHalfLowerSection">
                    {text}
                </div>
            </div>   
        )
    }
    const redirectToHome = () => {
        if (gameMode === 'computer') {
            updateHouseChoice('empty');
            setChoice('empty');
        } else {
            props.history.push(`/${gameType}`);
        }
    }
    const renderGameResult = () => {
        if(gameResult === null) {
            return(
                <div></div>
            );
        }

        return(
            <div className='gameResultText' id={'gameResultContainer'}>
                <span className="gameResultRuleText">
                    {gameResult.rule}
                </span>
                <span>
                    {gameResult.text}
                </span>
                <button 
                    type="button" 
                    className="btn playAgainButton"
                    onClick={() => redirectToHome()}
                >
                    PLAY AGAIN
                </button>     
            </div>  
        );
    }

    return(
      <div className="rpsRow result">
          <div className="rpsChoice hv-center">
              {renderChoiceComponent(userChoice, gameMode === 'computer' ? 'THE COMPUTER PICKED' :' YOU PICKED')}
              {renderChoiceComponent(houseChoice, 'THE COMPUTER PICKED')}
          </div>
          <div className="gameResult hv-center">
              {renderGameResult()} 
          </div>
      </div>
    )
}

export default withRouter(Result);
