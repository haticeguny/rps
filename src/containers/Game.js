import React, { useEffect, useState } from 'react';
import { withRouter } from "react-router-dom";

import { useGameMode } from '../contexts/GameMode';
import { useScore } from '../contexts/Score';

import loading from '../assets/images/loading.gif';


const Game = (props) => {
  const { updateGameMode } = useGameMode();
  const { updateScore } = useScore();
  const [gameMode, setGameMode] = useState('');

  useEffect(() => {
    updateScore(0, 'basic');
  }, []);

  useEffect(() => {
    const timer = setTimeout(() => {
      return <img src={loading} alt="loading..." />;
      }, 800);

    return () => clearTimeout(timer);
  }, []);

  const handleGameChange = (selectedGameMode) => {
    updateGameMode(selectedGameMode);
    setGameMode(selectedGameMode);

    if (selectedGameMode === 'player') {
      props.history.push({
        pathname: "/basic"
      });
    }

    if (selectedGameMode === 'computer') {
      props.history.push({
        pathname: "/basic/choice",
        state: { gameType: "basic" }
      });
    }
  }

  return (
    <>
      {!gameMode && (
        <div className="rpsGameMode">
          <div className="rpsGameMode-text">Please Select Your Game Mode</div>
          <div>
            <button type='button' className="btn rpsGameMode-player" onClick={() => handleGameChange('player')}>Player Vs Computer</button>
            <button type='button' className="btn rpsGameMode-computer" onClick={() => handleGameChange('computer')}>Computer Vs Computer</button>
          </div>
        </div>
      )}
    </>
  )
};

export default withRouter(Game);
